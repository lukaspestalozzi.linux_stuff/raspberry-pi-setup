# SSH (on other machine)
ssh-copy-id -i ~/.ssh/id_rsa.pub pi@192.168.1.20

# make rest as sudo
sudo su

# Update
apt-get update -y && apt-get upgrade -y

# Install essentials
apt-get install git

# Tailscale
curl -fsSL https://tailscale.com/install.sh | sh

# Swap Space (using dphys-swapfile)
swapon --show
nano /etc/dphys-swapfile
dphys-swapfile install
dphys-swapfile swapon
swapon --show

# Make sure dns works:
nano /etc/resolv.conf
# search tail1d0e9.ts.net lan
# nameserver 100.100.100.100
# nameserver 192.168.1.40
# nameserver 8.8.8.8

# Install Docker
curl -fsSL https://get.docker.com | sudo sh
usermod -aG docker pi
apt-get -y install docker-compose-plugin
